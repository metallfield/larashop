<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Product;
use App\Repositories\CommentsRepository;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @var \App\Repositories\CommentsRepository
     */
    private $commentsRepository;

    public function __construct()
    {
        $this->commentsRepository = app(CommentsRepository::class);
    }

    public function comments(Product $product)
    {
        $comments = $this->commentsRepository->getComments($product);
        return response()->json($comments);
    }

    public function create(Request $request)
    {
        $data = collect($request->all());
        $comment = $this->commentsRepository->createComment($data);
        return response()->json(['status' => 'success', 'comment' => $comment]);
    }
}

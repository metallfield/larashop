<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Product;
use App\Repositories\ProductRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\UserNotDefinedException;
use App\Category;

class ProductsController extends Controller
{
    /**
     * @var ProductRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $productRepository;

    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
    }

    public function getN(Request $request)
    {
        return response()->json($request->all());
    }
    public function getProd(Request $request)
    {

        $products = $this->productRepository->getAllProducts(auth()->user(), $request->category ? $request->category : null);

        return response()->json($products);
    }
}

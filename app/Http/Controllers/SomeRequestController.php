<?php

namespace App\Http\Controllers;

use App\Services\CsvRead;
use App\Services\CsvWrite;
use App\Services\ThrottleService;
use App\Services\UsersRequestServise;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SomeRequestController extends Controller
{

    /**
     * @var ThrottleService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $ips = [];
    /**
     * @var \App\Services\UsersRequestServise
     */
    private $userService;

    public function __construct()
    {

    }

    public function someRequest()
    {

        CsvWrite::writeTo('/home/dudos/Desktop/file.csv');
        $usersList = CsvRead::read('/home/dudos/Desktop/file.csv');

        $result = (new UsersRequestServise($usersList))->sendRequestsToApi();
        
        return  response()->json($result);
    }
}

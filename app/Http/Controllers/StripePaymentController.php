<?php

namespace App\Http\Controllers;

 use App\Http\Requests\OrderRequest;
 use App\Mail\OrderConfirm;
 use App\Order;
 use App\Repositories\StripeRepository;
 use App\Services\Basket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
 use Illuminate\Support\Facades\Mail;
 use Stripe;
class StripePaymentController extends Controller
{

    /**
     * @var \App\Repositories\StripeRepository
     */
    private $stripeRepository;

    public function __construct()
    {
        $this->stripeRepository = app(StripeRepository::class);
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {

        $email = Auth::check() ? Auth::user()->email : $request->email;
        $name = Auth::check() ? Auth::user()->name : $request->name;
        $billing_address = $request->address ? $request->address : null;
        $shipping = $request->shipping_address ? $request->shipping_address : null;
        $data = collect($request->all());
        $this->stripeRepository->createPayment($data);
        $order_id = session('orderId');
        $success = (new Basket(Auth::user()))->saveOrder($name, $billing_address, $email, $shipping, $order_id, $request->amount);
        if ($success) {
            Mail::to($email)->send(new OrderConfirm(Order::find($order_id)));
            session()->forget('orderId');
            session()->flash('success', 'Payment successful!');
            session()->forget('full_order_sum');
            return redirect()->route('orders.index');
        }else{
            session()->flash('warning', 'шо то не так');
            session()->forget('full_order_sum');
            return redirect()->back();
        }

    }


}

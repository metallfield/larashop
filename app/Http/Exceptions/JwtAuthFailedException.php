<?php


namespace App\Http\Exceptions;


use Illuminate\Http\Response;
use Exception;
class JwtAuthFailedException extends Exception
{

    /**
     * JwtAuthFailedException constructor.
     * @param string $string
     */
    public function render($request)
    {
        return response([
            'message' => 'Пользователь не авторизован',
            'errors' => [$this->getMessage()],
            'type' => class_basename($this),
        ], Response::HTTP_UNAUTHORIZED);

    }
}

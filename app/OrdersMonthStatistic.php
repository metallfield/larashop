<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersMonthStatistic extends Model
{
    protected $fillable = ['year', 'month', 'day', 'count_of_orders'];
}

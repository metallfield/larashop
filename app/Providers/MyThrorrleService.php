<?php

namespace App\Providers;

use App\Services\ThrottleService;
use Illuminate\Support\ServiceProvider;

class MyThrorrleService extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
            $this->app->singleton(ThrottleService::class, function ($app){
                return $this->app->instance(ThrottleService::class, new ThrottleService());
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}

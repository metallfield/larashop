<?php


namespace App\Repositories;


use App\Category;

class CategoriesRepository
{
    public function getAllCategories()
    {
        return Category::with('products')->get();
    }

    public function createCategory($data)
    {
        $category = new Category();
        $category->name = $data['name'];
        $category->description = $data['description'];
        $category->image = $data['image'];
        return $category->save($data);
    }

    public function editCategory($data, $category)
    {
        $category->description = $data['description'];
        $category->name = $data['name'];
        $category->image = $data['image'];
        return $category->save();
    }

    public function getCategoriesStatistic()
    {
        $categories = Category::select('id', 'name')->with('products')->get();
        $result = [];
        foreach ($categories as $category)
        {
            $result[] = ['name'=> $category->name, 'countProducts' => $category->products->count() ];
        }
        return $result;
    }
}

<?php


namespace App\Repositories;


use App\Comment;
use App\Product;

class CommentsRepository
{

    public function createComment($data)
    {
        $comment = Comment::create($data->toArray());
        return $comment->load('user', 'product');
    }

    public function getComments(Product $product)
    {
        return $product->comments ? $product->comments->load('user') : null;
    }
}

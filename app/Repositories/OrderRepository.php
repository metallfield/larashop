<?php


namespace App\Repositories;


use App\Order;
use App\OrdersMonthStatistic;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
    }

    public function create($data)
    {
        return  Order::create($data);
    }
    public function findOrder($orderId)
    {
            return   Order::findOrFail($orderId);
    }
    public function getMyOrders(User $user)
    {
        return  $user->orders()->Active()->paginate(6);
    }

    public function saveOrder($name, $address, $email, $shipping = null, $order_id, $amount)
    {
        $order = Order::where('id', $order_id)->first();
        if ($order->status == 0) {
            $order->name = $name;
            $order->address = $address;
            $order->email = $email;
            $order->shipping_address =  $shipping ? $shipping : null;
            $order->user_id = Auth::id();
            $order->status = 1;
            $order->amount = $amount;
            $order->save();
            $this->incrementCountOfOrdersByMonth();
            return true;
        }else{
            return false;
        }
    }
    protected function incrementCountOfOrdersByMonth()
    {
        $result = OrdersMonthStatistic::where(['year' => now()->year, 'month' => now()->month])->first();
        if (!empty($result))
        {

            $result->increment('count_of_orders');
            $result->save();
        }else{
            $data = [];
            $data['year'] = now()->year;
            $data['month'] = now()->month;
            $data['count_of_orders'] = 1;
            OrdersMonthStatistic::create($data);
        }
    }
    public function getTotalOrdersSum()
    {
       return Order::select('amount')->Active()->sum('amount');
    }

    public function getCountOfOrders()
    {
        return Order::count();
    }

    public function getOrdersByIds($ids)
    {
        return Order::whereIn('id', $ids)->with('products')->get();
    }

    public function  incomingOrders(User $user)
    {
     return    DB::table('order_product')->select('order_id')->where('owner', '=', $user->id)->groupBy('order_id')->paginate(6);
     //   return $this->getOrdersByIds(array_keys($ordersIds));
    }
}

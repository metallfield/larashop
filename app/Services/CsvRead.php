<?php


namespace App\Services;


class CsvRead
{
    public static function read($csvFile)
    {
        return CsvRead::readCSV($csvFile);
    }
    private static function readCSV($csvFile){
        $file_handle = fopen($csvFile, 'r');
        fgetcsv($file_handle, 0, ',');

// массив, в который данные будут сохраняться
        $data = [];
        while (($row = fgetcsv($file_handle, 0, ',')) !== false) {
            list($id, $login, $age) = $row;

            $data[] = [
                'ip' => $id,
                'login' => $login,
                'password' => $age
            ];
        }
        return $data;
    }
}

<?php


namespace App\Services;


class CsvWrite
{
    public static function writeTo($path)
    {
        if (filesize($path) == 0)
        {
            $fp = fopen($path, 'w');
            for ($i = 1; $i <=255; $i++){
                $arr = ['ip' =>
                    '192.0.0.'.$i,
                    'name' =>
                    CsvWrite::generateName(),
                    'password' =>
                    CsvWrite::generatePassword(),

                ];
                fputcsv($fp, $arr);
            }
            fclose($fp);
        }
    }


    private static function generateName()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 10);
    }
    private static function generatePassword()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 10);
    }


}

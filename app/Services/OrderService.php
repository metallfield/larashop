<?php


namespace App\Services;


use App\Product;
use App\Repositories\OrderRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getMyOrders(User $user)
    {
        return $this->orderRepository->getMyOrders($user);
    }
    public function getIncomingOrders(User $user)
    {
        $orders = $this->orderRepository->incomingOrders($user);
        return $orders ? $orders :null;
    }
}

<?php


namespace App\Services;


use App\Repositories\CategoriesRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;

class StatisticService
{

    /**
     * @var \App\Repositories\ProductRepository
     */
    private $productRepository;
    /**
     * @var \App\Repositories\OrderRepository
     */
    private $orderRepository;
    /**
     * @var \App\Repositories\CategoriesRepository
     */
    private $categoriesRepository;

    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
        $this->orderRepository = app(OrderRepository::class);
        $this->categoriesRepository = app(CategoriesRepository::class);
    }

    public function getMostTrendingProduct(){
        $mostSails = $this->productRepository->getMostTrendingProduct();
        $mostSailsArray = [];
        $mSails = [];
        foreach ($mostSails as $ms)
        {
            $mostSailsArray[] = $ms->product_id;
            $mSails[]  = ['id' => $ms->product_id, 'count' => $ms->count];
        }
        $products = $this->productRepository->getProductsByIds($mostSailsArray) ;

            foreach ($products as $product)
            {
                $key = array_search($product->id, array_column($mSails, 'id'));
                $pr[] = ['product' => $product, 'count' => $mSails[$key]['count']];
            }
        return ['products' => $pr];
    }
    public function getProductStatistic()
   {
       $sum = $this->productRepository->getStatistic();
        $count = $this->productRepository->getProductsCount();
       return ['sum' => $sum, 'count' => $count];
   }

   public function getOrdersStatistic()
   {
       return $this->productRepository->getOrdersStatistic();
   }
   public function getTotalOrdersSum()
   {
       $price = $this->orderRepository->getTotalOrdersSum();
       $count = $this->orderRepository->getCountOfOrders();
       return ['price' => $price, 'count' => $count];
   }

   public function getCategoriesStatistic()
   {
       return $this->categoriesRepository->getCategoriesStatistic();
   }
}

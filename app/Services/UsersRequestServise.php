<?php


namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class UsersRequestServise
{
    private $users;
    protected $current_user_index = 0;
    protected $count = 0;
    public function __construct($usersList)
    {
        $this->users = $usersList;
    }

    public function sendRequestsToApi()
    {

        if ($this->count < 600)
        {
            $user = $this->users[$this->current_user_index];
            $client = new Client();
            $ip = $user['ip'];
            try {
                $response = $client->request('GET', 'http://shop.loc/api/some-api-response/' . $ip);
            } catch (GuzzleException $e) {
                return  response()->json('error');
            }
            $result = $response->getBody();
            if ($response->getStatusCode() == 200)
            {
                $this->count++;
            }

        }else{
            $this->current_user_index +=1;
            $this->count = 0;
        }

        return $response->getBody()->getContents();
    }
}

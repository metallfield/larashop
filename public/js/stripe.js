$(document).ready(function() {

    const stripe = Stripe('pk_test_51H81o9DYZL8ObeUkgU1y2OSfSVH4LSuAr2Vuc28iXMPar5XZJbWlgW5koujWCjCnpOq7GIHYvuTvPTRxpGyeWDB100OlVBdspx');

    const elements = stripe.elements();
    const card = elements.create('card');

    card.mount('#card-element');
    let form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the customer that there was an error.
                let errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        let form = document.getElementById('payment-form');
        let hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }


});

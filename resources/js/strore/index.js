
import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex);

export default {
    state: {
        products: {
            products: {
                cats: []
            }
        },
        categories: [],
        basket: {
            products: [],
        },
        incomingOrders: [],
        pagination: []
    },

    actions : {
        getAllProducts(context, data){
                     axios.post('/api/prod?page='+data.page, {category: data.category})
                        .then((response)=>{
                            console.log('dispatching success');
                            console.log(response.data);
                            context.commit("product", response.data.data);
                            context.commit("prod_cats");
                            context.commit("pagination", response.data);

                        })
        },
        getAllCategories(context, page){
            axios.get('/getCategories')
                .then((response)=>{
                    console.log(response.data)
                    context.commit("categories", response.data);
                })
        },
        getCategoryProduct(context, id)
        {
            axios.get('/api/categoryProducts/'+id)
                .then((response)=>{
                    context.commit('catProd', response.data.data);
                 })
        },
        getBasketProducts(context)
        {
            axios.get('/getBasket/')
                .then((response)=>{
                    console.log(response.data);
                    context.commit("basket", response.data);
                })
        },
        addProduct(context, product)
        {
            axios.post('/basket/add/'+ product.id)
                .then((response)=>{
                    console.log(response);
                 })
        },
        remove_product(context, product)
        {
            axios.post('/basket/remove/'+ product.id)
                .then((response)=>{
                    console.log(response);
                 })
        },
        getIncomingOrders(context, page){
            axios.get('/getIncomingOrders?page='+page)
                .then((response)=>{
                    console.log(response.data)
                    context.commit("pagination", response.data);
                    axios.post('/api/incomingOrders', {
                        ids: response.data.data
                    }).then((response)=>{
                        console.log(response.data);
                        context.commit('setIncomingOrders', response.data);
                    })

                })
        }
    },
    getters : {
      getBasket: state => {
          return state.basket.products;
      },
        basketTotal: state => {
          return state.total;
        },

    },
    mutations: {
        product(state, data){
            return state.products = data;
        },

        prod_cats(state)
        {
          state.products.forEach((product)=>{
              let cats = []
              product.categories.forEach((category)=>{
                  cats.push(category.name);
              })
              product.cats = cats;
          })
        },
        pagination(state, data)
        {
            return state.pagination = data;
        },
        catProd(state, data)
        {
            return state.products = data;
        },
        categories(state, data){
            return state.categories = data;
        },
        basket(state, data) {
            return state.basket = data;
        },
        setIncomingOrders(state, data){
            return state.incomingOrders = data;
        }

    }
};


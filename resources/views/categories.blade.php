<?php
?>
@extends('layouts.app')
@section('title', 'categories ')
@section('content')
<div class="container">
    <h2>category list:</h2>
    <ul class="list-unstyled"></ul>
    @foreach($categories as $category)
        <li class="p-3 border list-unstyled row">
            <div class="col">
            <img src="{{\Illuminate\Support\Facades\Storage::url($category->image)}}" alt="" height="250px">
            </div>
            <div class="col">
            <a href="{{route('category-show', [$category])}}">{{$category->name}}</a>
                <p class="text-dark">{{$category->description}}</p>
            </div>
        </li>
    @endforeach

</div>

@endsection

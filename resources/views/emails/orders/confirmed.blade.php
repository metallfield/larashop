@component('mail::message')
# Introduction

The order confirmed !
Total sum {{$order->amount}}

@component('mail::button', ['url' => route('orders.show', [$order->id])])
Show Order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group([
    'middleware' => ['auth:api'],

], function ($router) {

    Route::post('/comments', 'Api\CommentsController@create');
    Route::post('logout', 'Api\AuthController@logout')->middleware('auth:api');
//    Route::post('refresh', 'Api\AuthController@refresh');

});

 Route::post('/prod', 'Api\ProductsController@getProd');

        Route::post('/incomingOrders', 'OrderController@incom');
        Route::get('/categoryProducts/{category}', 'CategoriesController@getCategoryProducts');

     Route::post('login', 'Api\AuthController@login')->name('api.log');
    Route::post('register', 'Api\AuthController@register');

Route::get('/comments/{product}', 'Api\CommentsController@comments');

